### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 5910acf2-4f51-4967-938b-2d97b031b537
board = [[" ", " ", " "],
         [" ", " ", " "],
         [" ", " ", " "]]

# ╔═╡ 8e01496e-26e6-47a0-b43a-17d2d2caa4fa
player1 = "X"

# ╔═╡ 3508d236-2231-471c-acd9-48161a577a83
player2 = "O"

# ╔═╡ 392ad700-c837-4925-90bc-a5f6f160b755
player1_win = -1

# ╔═╡ ee12a6ba-9716-4b1c-a587-6c2497b01a52
tie = 0

# ╔═╡ 486db303-d474-4b44-a166-78c171a49eb5
player2_win = 1

# ╔═╡ 44fa2773-344e-4aa1-b54d-14d7c678517b
function print_board(board)
    println("    1   2   3")
    println(" 1  ", join(board[1], " | "))
    println("   ———┼———┼———")
    println(" 2  ", join(board[2], " | "))
    println("   ———┼———┼———")
    println(" 3  ", join(board[3], " | "))
end

# ╔═╡ 38d6dcc6-ae8e-4d79-a6b4-85e5e9916ab3
function make_move!(board, player, (x, y))
    board[x][y] = player
end

# ╔═╡ e6913912-e47d-43d4-a4d1-231984ece613
function check_available(board)
    available_cells = []
    for i in 1:3, j in 1:3
        if board[i][j] == " "
            push!(available_cells, (i, j))
        end
    end
    return available_cells
end

# ╔═╡ 0c2658f5-babb-4d18-b30e-0222979fef85
function check_win(board)

    # Rows
    for i in 1:3
        if (board[i][1] == board[i][2] == board[i][3]) && (board[i][1] in [player1, player2])
            if (board[i][1] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Column
    for i in 1:3
        if (board[1][i] == board[2][i] == board[3][i]) && (board[1][i] in [player1, player2])
            if (board[1][i] == player1)
                return player1_win
            end
            return player2_win
        end
    end

    # Principal diagonal
    if (board[1][1] == board[2][2] == board[3][3]) && (board[1][1] in [player1, player2])
        if (board[1][1] == player1)
            return player1_win
        end
        return player2_win
    end

    # Other diag
    if (board[1][3] == board[2][2] == board[3][1]) && (board[1][3] in [player1, player2])
        if (board[1][3] == player1)
            return player1_win
        end
        return player2_win
    end

    # Tie
    if length(check_available(board)) == 0
        return tie
    end

    # No one wins, game still goes on
    return nothing
end

# ╔═╡ 9658192a-5478-47ee-95c0-c27697623ea7
function minimax(board, depth, is_maximising)
    result = check_win(board)

    if result !== nothing
        return result
    elseif is_maximising

        best_score = -Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = player2

                score = minimax(board, depth + 1, false)
                
                board[i][j] = " "

                best_score = max(score, best_score)
            end
        end

        return best_score        
    else        
        best_score = Inf

        for i in 1:3, j in 1:3
            if board[i][j] == " "

                board[i][j] = player1

                score = minimax(board, depth + 1, true)
                
                board[i][j] = " "

                best_score = min(score, best_score)
            end
        end

        return best_score   
    end

end

# ╔═╡ 01ca6fe4-57a5-4ce3-941f-519fd59624a9
function worst_move(board)
    worst_score = Inf

    worst_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = player2
            
            score = minimax(board, 0, true)

            
            board[i][j] = " "

            if score < worst_score
                worst_score = score
                worst_pos = (i, j)
            end
        end
    end
    return worst_pos
end

# ╔═╡ d8a464f0-f0f4-430e-9e43-30c93129f545
function best_move(board)
    best_score = -Inf

    best_pos = (0, 0)

    for i in 1:3, j in 1:3
        if board[i][j] == " "

            board[i][j] = player2
            
            score = minimax(board, 0, false)

            
            board[i][j] = " "

            if score > best_score
                best_score = score
                best_pos = (i, j)
            end
        end
    end
    return best_pos
end

# ╔═╡ d42a964f-f02a-44c9-8937-a5dc3f1439de
function ai_move!(board, difficulty)
    if difficulty == "loser"
        make_move!(board, player2, worst_move(board))
    elseif difficulty == "easy"
        pos = rand(check_available(board))
        make_move!(board, player2, pos)
    elseif difficulty == "medium"
        if rand() < 0.3
            make_move!(board, player2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, player2, pos)
        end
    elseif difficulty == "hard"
        if rand() < 0.7
            make_move!(board, player2, best_move(board))
        else            
            pos = rand(check_available(board))
            make_move!(board, player2, pos)
        end
    elseif difficulty == "impossible"
        make_move!(board, player2, best_move(board))
    end
end

# ╔═╡ 474033f3-dd59-4a2e-9126-f1f234a73df1
function move_input(prompt, params)

    println(prompt)
    txt = split(readline())

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in string.(collect(1:3)) && txt[2] in string.(collect(1:3))
        return txt
    else
        println("Please enter a valid command (See README)")
        move_input(prompt, params)
    end
end

# ╔═╡ dd47b975-dd11-40ed-a780-59347978f324
function generalised_input(prompt, same_line, choices, params)
    same_line ? print(prompt) : println(prompt)

    txt = split(lowercase(readline()))

    if txt == ["exit"]
        exit()
    end

    if length(txt) == params && txt[1] in choices
        return txt
    else
        println("Please enter a valid command (See README)")
        generalised_input(prompt, same_line, choices, params)
    end
end

# ╔═╡ 699c24c9-8ccc-4fe5-89a7-4d6a34a344f8
print_board(board)

# ╔═╡ d2aef8ec-d0ee-4f68-b547-a5744d1bce68
diff_levels = """
loser - human always wins
easy - AI is random
medium - AI makes some mistakes
hard - AI makes less mistakes
impossible - AI is perfect
"""

# ╔═╡ 7ae2552e-6e31-4f2b-817f-a7632fc38222
println(diff_levels)

# ╔═╡ f18f0bb2-ac4b-4da0-88ba-981a38e5640f
difficulty = generalised_input("Difficulty: ", true, ["loser", "easy", "medium", "hard", "impossible"], 1)[1]

# ╔═╡ ed24cbd3-fc8a-422f-b772-8d0628cea82b
while true
    print_board(board)

    txt = move_input("Enter: ", 2)
    make_move!(board, player1, (parse(Int, txt[1]), parse(Int, txt[2])))

    if check_win(board) == player1_win
        println(1)
        break
    elseif check_win(board) == player2_win
        println(2)
        break
    elseif check_win(board) == tie
        println("Tie!")
        break
    end

    ai_move!(board, difficulty)
    print_board(board)

    if check_win(board) == player1_win
        println(1)
        break
    elseif check_win(board) == player2_win
        println(2)
        break
    elseif check_win(board) == tie
        println("Tie!")
        break
    end
end

# ╔═╡ b5f35d10-46eb-4c7e-96f2-5ba012049d3a


# ╔═╡ Cell order:
# ╠═5910acf2-4f51-4967-938b-2d97b031b537
# ╠═8e01496e-26e6-47a0-b43a-17d2d2caa4fa
# ╠═3508d236-2231-471c-acd9-48161a577a83
# ╠═392ad700-c837-4925-90bc-a5f6f160b755
# ╠═ee12a6ba-9716-4b1c-a587-6c2497b01a52
# ╠═486db303-d474-4b44-a166-78c171a49eb5
# ╠═0c2658f5-babb-4d18-b30e-0222979fef85
# ╠═9658192a-5478-47ee-95c0-c27697623ea7
# ╠═01ca6fe4-57a5-4ce3-941f-519fd59624a9
# ╠═d8a464f0-f0f4-430e-9e43-30c93129f545
# ╠═44fa2773-344e-4aa1-b54d-14d7c678517b
# ╠═38d6dcc6-ae8e-4d79-a6b4-85e5e9916ab3
# ╠═e6913912-e47d-43d4-a4d1-231984ece613
# ╠═d42a964f-f02a-44c9-8937-a5dc3f1439de
# ╠═474033f3-dd59-4a2e-9126-f1f234a73df1
# ╠═dd47b975-dd11-40ed-a780-59347978f324
# ╠═699c24c9-8ccc-4fe5-89a7-4d6a34a344f8
# ╠═d2aef8ec-d0ee-4f68-b547-a5744d1bce68
# ╠═7ae2552e-6e31-4f2b-817f-a7632fc38222
# ╠═f18f0bb2-ac4b-4da0-88ba-981a38e5640f
# ╠═ed24cbd3-fc8a-422f-b772-8d0628cea82b
# ╠═b5f35d10-46eb-4c7e-96f2-5ba012049d3a
