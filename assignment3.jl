### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ c2f3da30-d9ae-11eb-161c-f3bddacbf7cf
using Pkg

# ╔═╡ 3bf43f19-7f3c-41ac-9034-d6ddda76f8d3
Pkg.add("POMDPs")

# ╔═╡ 36178b1b-8d11-45fb-9743-b6917560c79a
Pkg.add("POMDPModelTools")

# ╔═╡ 2c03b191-f0be-44ba-841f-4d9f489a7ca9
Pkg.add("POMDPPolicies")

# ╔═╡ 4a110cc5-d50d-4ad3-b0cd-9d8682d6b7d2
Pkg.add("POMDPSimulators")

# ╔═╡ f052577e-9b4d-4ae8-8f5f-cb335a210957
Pkg.add("DiscreteValueIteration")

# ╔═╡ af81f769-d9c2-42f8-84e0-9eb3e718ca7f
Pkg.add("MCTS")

# ╔═╡ c0c9c0d4-382e-4b5c-b3f2-cadec9d5e8b5

using POMDPs

# ╔═╡ be8a1866-205c-49bf-bf48-f85a09d8748a
using MCTS

# ╔═╡ 7a804dbe-6702-42e6-ad3b-64f19a783ba2
# this package has what we'll use to initialise MDP
using POMDPModelTools

# ╔═╡ 89aa6719-395a-411b-8c10-72883b959058
# for the polies for the MDP
using POMDPPolicies

# ╔═╡ c4a35eee-aea7-4f36-82ce-5ee2328a760f

using POMDPSimulators

# ╔═╡ 16b74bdf-21e5-419c-807e-6b99d50adbb1
using DiscreteValueIteration

# ╔═╡ b44a67d0-7bf2-49b8-a8e3-69791d428d2f
md" ### The packages that we'll be using in this project"

# ╔═╡ 4bfb3834-14b4-4525-9587-647d521342ea
md""" ### Problem defenition 
  
A Grid world which has a 10x10 grid, The states are represented by each position in the grid. There are 4 terminal states, 2 are positive, 2 are negative. The model has 4 actions :up :down :left :right


"""

# ╔═╡ 44e83601-3f2e-42a1-abb6-1d482ff40818
abstract type TheMDP <: MDP{Int64, Int64} # MDP{State, Type}

end

# ╔═╡ 1cd3cce3-add4-411d-ad0a-296bf16fa29f
md""" #### The state of an agent in the Gridworld 

"""

# ╔═╡ be2fcbc6-1cd5-45d0-8824-115ae9872073
struct GridWorldState 
    x::Int64 # x position
    y::Int64 # y position
    done::Bool # checks if they are in the terminal state?
end

# ╔═╡ 0303c9bc-ffd3-4748-b76f-d3cc7ff3e9c5
#the constructor constructor
gridWorldState(x::Int64, y::Int64) = GridWorldState(x,y,false)

# ╔═╡ 5f873593-d1f3-4f37-b046-f295337363e8
# compare two states
posequal(state1::GridWorldState, state2::GridWorldState) = state1.x == state2.x && state1.y == state2.y

# ╔═╡ 0667ce67-ce47-4552-8511-3fc68a5e4916
md"""  The GridWorld defenition """

# ╔═╡ 97dff0c4-9656-49bd-bca1-04b0d7a5571d
# our grid world mdp type
mutable struct GridWorld <: MDP{GridWorldState, Symbol} # Note that our MDP is parametarized by the state and the action
    size_x::Int64 # x axis size 
    size_y::Int64 # the y axis 
    reward_states::Vector{GridWorldState} # the reward states
    reward_values::Vector{Float64} 
    tprob::Float64 # probability of transition probability
    discount_factor::Float64 # the disocunt factor
end

# ╔═╡ ae49cf11-65cf-45b7-8d22-3656c860a046
#we use key worded arguments so we can change any of the values we pass in 
function gridWorld(;sx::Int64=10, # size of the grid
                    sy::Int64=10, 
                    rs::Vector{GridWorldState}=[gridWorldState(4,3), gridWorldState(4,6), gridWorldState(9,3), gridWorldState(8,8)], # reward states
                    rv::Vector{Float64}=rv = [-10.,-5,10,3], # reward values for RS
                    tp::Float64=0.7, 
                    discount_factor::Float64=0.9)
    return GridWorld(sx, sy, rs, rv, tp, discount_factor)
end

# ╔═╡ b8630cc8-6735-4eb3-83da-bf42439c69d9
# creating a default Gridworld with values.
mdp = gridWorld()

# ╔═╡ af24563c-946d-4d5a-8b52-fc573c5c1386
md"""For testing """

# ╔═╡ 2a0fc9a4-9a9c-4260-8936-d38e7b2fb28e
mdp.reward_states # for testing

# ╔═╡ 51268750-5c33-4731-8fca-e6d1d763b828
md""" #### This method returns an array of all our states """

# ╔═╡ a1171da9-3410-49f9-b7d1-d102360d249e
function POMDPs.states(mdp::GridWorld)
    states = GridWorldState[] # an array of GridWorldStates

    for d = 0:1, y = 1:mdp.size_y, x = 1:mdp.size_x
        push!(states, GridWorldState(x,y,d))
    end
    return states
end

# ╔═╡ a890e8bc-ad50-4ca8-b501-f483f76b5b5a
md""" ##### For testing purposes
 + loading one state 

"""

# ╔═╡ 5a82bcfc-b213-4460-8576-caf5296aade7
state_space = states(mdp);

# ╔═╡ 4b8f675e-1786-4032-af95-37f6a85d3682
state_space[1]

# ╔═╡ f34a4a16-72f8-4518-b5e0-0fba8412c4ac
md"""All the actions the agent can take
  + up
  + down
  + left
  + right
"""

# ╔═╡ 4c296eed-d662-4fee-ad4c-e78a4f09c6c8
POMDPs.actions(mdp::GridWorld) = [:up, :down, :left, :right];

# ╔═╡ 68b5d392-a422-4013-a6cf-fb0e23d6a03e
md""" #### Transition Model
T(s' | s,a)

 Since in our case there are a few states that have nonzero probability, we,ll use sparseCat from POMDPToolbox module.

"""

# ╔═╡ 75dc419c-7882-4c96-8f16-49aac3a0aa44
# transition helpers
function inbounds(mdp::GridWorld,x::Int64,y::Int64)
    if 1 <= x <= mdp.size_x && 1 <= y <= mdp.size_y
        return true
    else
        return false
    end
end

# ╔═╡ de033c07-74b0-4b15-8f7d-ada42aa8cbf0


# ╔═╡ 31a53530-a863-422c-b6e1-6a6c3fa0895f
inbounds(mdp::GridWorld, state::GridWorldState) = inbounds(mdp, state.x, state.y);

# ╔═╡ c711b3ba-b3ea-48e3-9574-26202c2e509e
md""" #### This function returns s' probability given the (a,s) """

# ╔═╡ a05ffa8f-5230-4e19-91df-b83a20ed3e88
function POMDPs.transition(mdp::GridWorld, state::GridWorldState, action::Symbol)
    a = action
    x = state.x
    y = state.y
    
	#return the gridworldstate if in terminal states
    if state.done
        return SparseCat([GridWorldState(x, y, true)], [1.0])
    elseif state in mdp.reward_states
        return SparseCat([GridWorldState(x, y, true)], [1.0])
    end

    neighbors = [
        GridWorldState(x+1, y, false), # move right
        GridWorldState(x-1, y, false), # move left
        GridWorldState(x, y-1, false), # move down
        GridWorldState(x, y+1, false), # move up
        ] 
    
    targets = Dict(:right=>1, :left=>2, :down=>3, :up=>4)
    target = targets[a]
    
    probability = fill(0.0, 4)

    if !inbounds(mdp, neighbors[target])
        # checks If we are not transitioning out of bounds, if that happens, stay in
        # that cell with probability 1
        return SparseCat([gridWorldState(x, y)], [1.0])
    else
        probability[target] = mdp.tprob

        oob_count = sum(!inbounds(mdp, n) for n in neighbors) 

        new_probability = (1.0 - mdp.tprob)/(3-oob_count)

        for i = 1:4 
            if inbounds(mdp, neighbors[i]) && i != target
                probability[i] = new_probability
            end
        end
    end

    return SparseCat(neighbors, probability)
end;

# ╔═╡ 5d87c5e2-7420-4cae-9fea-d422eff7182b
md""" #### The reward function 
R(s,a,s')
+ The agent is only rewarded if it gets into the terminal state
"""

# ╔═╡ ffcda3d7-7080-471f-bc1f-2c8a3eb8ceeb
function POMDPs.reward(mdp::GridWorld, state::GridWorldState, action::Symbol, statep::GridWorldState) 
    if state.done
        return 0.0
    end
    reward = 0.0
    n = length(mdp.reward_states)
    for i = 1:n
        if posequal(state, mdp.reward_states[i])
            reward += mdp.reward_values[i]
        end
    end
    return reward
end

# ╔═╡ b697e694-78b4-486f-9294-1196f7edf5b1
md""" #### The Discount Function 
"""

# ╔═╡ 82306c56-1cfe-4de5-9061-e9cd1bab86de
POMDPs.discount(mdp::GridWorld) = mdp.discount_factor;

# ╔═╡ 66b5f4ae-6cde-46d6-948c-22794f6bc648
md""" Indexing functions 


"""

# ╔═╡ 02f05a2d-c859-485c-a6a2-3002ea2d0aaf
function POMDPs.stateindex(mdp::GridWorld, state::GridWorldState)
    sd = Int(state.done + 1)
    ci = CartesianIndices((mdp.size_x, mdp.size_y, 2))
    return LinearIndices(ci)[state.x, state.y, sd]
end

# ╔═╡ ff6207a2-0a89-4132-a6f2-253e071d7214
function POMDPs.actionindex(mdp::GridWorld, act::Symbol)
    if act==:up
        return 1
    elseif act==:down
        return 2
    elseif act==:left
        return 3
    elseif act==:right
        return 4
    end
    error("Invalid GridWorld action: $act")
end;

# ╔═╡ 971b2f98-cb85-4897-b999-892417a24d8a
md""" #### checking if the agent is in the terminal state 


"""

# ╔═╡ ffc28e49-11d4-4a76-8476-4a40e6c6abc6
POMDPs.isterminal(mdp::GridWorld, s::GridWorldState) = s.done

# ╔═╡ babfdbb4-9150-436f-ae89-028e7ba06005
POMDPs.initialstate(pomdp::GridWorld) = Deterministic(gridWorldState(1,1)) # init the starting state

# ╔═╡ 2c402f29-7e25-447c-b0e0-737527737026
mdp.tprob=1.0

# ╔═╡ df59d683-55ec-4512-bbfd-a26ec39e0c58
md""" #### Setting the Policy using POMDPPolicies module 


"""

# ╔═╡ 21001700-5aa9-4df5-a88f-e48b1bf7c2d7
policy = RandomPolicy(mdp)

# ╔═╡ e671030e-7416-45a9-a150-f2ca85efc53b
left_policy = FunctionPolicy(s->:left)

# ╔═╡ 0ee5f47b-073f-4078-9a7c-d84fabf1934b
right_policy = FunctionPolicy(s->:right)

# ╔═╡ 878f639b-2190-4c79-82f1-3729d7b9055e
md""" #### POMDPSimulators  allows us to stimulate the problem to see if its working"""

# ╔═╡ 88ccbd33-373f-4852-98fc-7ed11395c308
for (s,a,r) in stepthrough(mdp, right_policy, "s,a,r", max_steps=10)
    @show s
    @show a
    @show r
    println()
end

# ╔═╡ 7d9aa4c1-7d1a-44fb-88f8-4211cc8c1835
md""" #### Using Value iteration to get an optimal action for a given state """

# ╔═╡ 27a2c3f1-4113-4e85-ba18-81080df789f0
# initialize the solver
# max_iterations: max iterations value iteration runs for
# belres: the value of Bellman residual used in the solver (defualt is 1e-3)
solver = ValueIterationSolver(max_iterations=100, belres=1e-3; verbose=true)

# ╔═╡ 6ccfd8de-9a2e-4ee7-81ae-437d24601113
# solve for an optimal policy
policy_two = solve(solver, mdp);

# ╔═╡ c9b43b6a-6542-42fb-b630-39e41c4d7b95
# given a state
s = gridWorldState(9,2)

# ╔═╡ 75d39cc4-71d0-4dbc-8fab-cd861a3215c4
#given an action
a = action(policy, s)

# ╔═╡ 9192741e-bde8-41b0-b89d-77ef75bf1991
for (s,a,r) in stepthrough(mdp, policy, "s,a,r", max_steps=20)
    @show s
    @show a
    @show r
    println()
end

# ╔═╡ 8e8810e5-e108-4f49-8a55-a24fe8f28bc3
md"""#### The monte carlo Solver """

# ╔═╡ 9b62e6d5-4db4-41ac-92ad-5de6ba573814
# initialize the solver with hyper parameters
# n_iterations: the number of iterations that each search runs for
# depth: the depth of the tree (how far away from the current state the algorithm explores)
# exploration constant: this is how much weight to put into exploratory actions. 
# A good rule of thumb is to set the exploration constant to what you expect the upper bound on your average expected reward to be.
solver_mct = MCTSSolver(n_iterations=1000,
                    depth=20,
                    exploration_constant=10.0,
                    enable_tree_vis=true)

# ╔═╡ 9a0ad544-ed44-4798-b649-0dbad6eb1695
# initialize the planner by calling the `solve` function. For online solvers, the 
planner = solve(solver, mdp)

# ╔═╡ f3d7aae5-b02d-4bcc-9af2-d7395a67a973
# to get the action:
state_mct = gridWorldState(9,2)

# ╔═╡ d8394077-0c4d-43b4-a539-28dbfcbe1421
action_mct = action(planner, state_mct)

# ╔═╡ Cell order:
# ╟─b44a67d0-7bf2-49b8-a8e3-69791d428d2f
# ╠═c2f3da30-d9ae-11eb-161c-f3bddacbf7cf
# ╠═3bf43f19-7f3c-41ac-9034-d6ddda76f8d3
# ╠═36178b1b-8d11-45fb-9743-b6917560c79a
# ╠═2c03b191-f0be-44ba-841f-4d9f489a7ca9
# ╠═4a110cc5-d50d-4ad3-b0cd-9d8682d6b7d2
# ╠═f052577e-9b4d-4ae8-8f5f-cb335a210957
# ╠═af81f769-d9c2-42f8-84e0-9eb3e718ca7f
# ╠═c0c9c0d4-382e-4b5c-b3f2-cadec9d5e8b5
# ╠═be8a1866-205c-49bf-bf48-f85a09d8748a
# ╠═7a804dbe-6702-42e6-ad3b-64f19a783ba2
# ╠═89aa6719-395a-411b-8c10-72883b959058
# ╠═c4a35eee-aea7-4f36-82ce-5ee2328a760f
# ╠═16b74bdf-21e5-419c-807e-6b99d50adbb1
# ╠═4bfb3834-14b4-4525-9587-647d521342ea
# ╠═44e83601-3f2e-42a1-abb6-1d482ff40818
# ╟─1cd3cce3-add4-411d-ad0a-296bf16fa29f
# ╠═be2fcbc6-1cd5-45d0-8824-115ae9872073
# ╠═0303c9bc-ffd3-4748-b76f-d3cc7ff3e9c5
# ╠═5f873593-d1f3-4f37-b046-f295337363e8
# ╟─0667ce67-ce47-4552-8511-3fc68a5e4916
# ╠═97dff0c4-9656-49bd-bca1-04b0d7a5571d
# ╠═ae49cf11-65cf-45b7-8d22-3656c860a046
# ╠═b8630cc8-6735-4eb3-83da-bf42439c69d9
# ╠═af24563c-946d-4d5a-8b52-fc573c5c1386
# ╠═2a0fc9a4-9a9c-4260-8936-d38e7b2fb28e
# ╟─51268750-5c33-4731-8fca-e6d1d763b828
# ╠═a1171da9-3410-49f9-b7d1-d102360d249e
# ╠═a890e8bc-ad50-4ca8-b501-f483f76b5b5a
# ╠═5a82bcfc-b213-4460-8576-caf5296aade7
# ╠═4b8f675e-1786-4032-af95-37f6a85d3682
# ╟─f34a4a16-72f8-4518-b5e0-0fba8412c4ac
# ╠═4c296eed-d662-4fee-ad4c-e78a4f09c6c8
# ╟─68b5d392-a422-4013-a6cf-fb0e23d6a03e
# ╠═75dc419c-7882-4c96-8f16-49aac3a0aa44
# ╠═de033c07-74b0-4b15-8f7d-ada42aa8cbf0
# ╠═31a53530-a863-422c-b6e1-6a6c3fa0895f
# ╠═c711b3ba-b3ea-48e3-9574-26202c2e509e
# ╠═a05ffa8f-5230-4e19-91df-b83a20ed3e88
# ╟─5d87c5e2-7420-4cae-9fea-d422eff7182b
# ╠═ffcda3d7-7080-471f-bc1f-2c8a3eb8ceeb
# ╟─b697e694-78b4-486f-9294-1196f7edf5b1
# ╠═82306c56-1cfe-4de5-9061-e9cd1bab86de
# ╟─66b5f4ae-6cde-46d6-948c-22794f6bc648
# ╠═02f05a2d-c859-485c-a6a2-3002ea2d0aaf
# ╠═ff6207a2-0a89-4132-a6f2-253e071d7214
# ╟─971b2f98-cb85-4897-b999-892417a24d8a
# ╠═ffc28e49-11d4-4a76-8476-4a40e6c6abc6
# ╠═babfdbb4-9150-436f-ae89-028e7ba06005
# ╠═2c402f29-7e25-447c-b0e0-737527737026
# ╟─df59d683-55ec-4512-bbfd-a26ec39e0c58
# ╠═21001700-5aa9-4df5-a88f-e48b1bf7c2d7
# ╠═e671030e-7416-45a9-a150-f2ca85efc53b
# ╠═0ee5f47b-073f-4078-9a7c-d84fabf1934b
# ╟─878f639b-2190-4c79-82f1-3729d7b9055e
# ╠═88ccbd33-373f-4852-98fc-7ed11395c308
# ╟─7d9aa4c1-7d1a-44fb-88f8-4211cc8c1835
# ╠═27a2c3f1-4113-4e85-ba18-81080df789f0
# ╠═6ccfd8de-9a2e-4ee7-81ae-437d24601113
# ╠═c9b43b6a-6542-42fb-b630-39e41c4d7b95
# ╠═75d39cc4-71d0-4dbc-8fab-cd861a3215c4
# ╠═9192741e-bde8-41b0-b89d-77ef75bf1991
# ╠═8e8810e5-e108-4f49-8a55-a24fe8f28bc3
# ╠═9b62e6d5-4db4-41ac-92ad-5de6ba573814
# ╠═9a0ad544-ed44-4798-b649-0dbad6eb1695
# ╠═f3d7aae5-b02d-4bcc-9af2-d7395a67a973
# ╠═d8394077-0c4d-43b4-a539-28dbfcbe1421
